# Statuts d'entreprise coopérative

## Préambule

Ces statuts contiendront beaucoup de naïvetés. Les mécanismes permettant collectivement de les faire progresser y sont présents.

L'activité de recherche scientifique requiert une certaine liberté vis-à-vis de l'économie de marché tout en pouvant en bénéficier. De la même manière, des formes de management hiérarchique peuvent brider la progression de projets scientifiques, bien que la coordination entre acteurs est un élément indispensable.

La présente société met en place un cadre favorable aux initiatives indépendantes de projets de recherche scientifique tout en promouvant la coordination et les activités économiques permettant la pérennité économique des projets.

Chaque membre de la société est employé·e sans lien de subordination afin de garantir l'indépendance de ses membres en termes de projets. L'établissement d'un contrat entre l'un·e des membres et un·e client·e passe par l'avis du collectif.

La mutualisation de moyens a pour but de donner une dynamique de projets désignés et réalisés collectivement. Les gains réalisés par le collectif ont pour vocation d'être employé pour des projets de recherche scientifique détachés des logiques de marché.


## Titre 1 - Stipulations générales

## Article 1 

La présente société est une Société Coopérative Ouvriére et de Production, sous la forme d'une Société à actions simplifiées.

La société est par ailleurs une Coopérative d'Activité et d'Emploi.


## Article 1 — Objet

Ce collectif est une coopérative et a pour vocation de porter les activités de recherche scientifique de ses membres et contribuer à la pérennisation d'activités de recherche en son sein et au-dehors.

Les activités de recherche scientifique et les activités à vocation économique cohabitent. Les activités à vocations économiques sont poursuivies par les membres de la société pour elles-mêmes et participent au bilan d'activité économique de la société. Les activités de recherche scientifique participent au bilan d'activité scientifique de la société.




## Article x — Membres


## Article 2 — Gouvernance

Les modes de gouvernance visent à donner le plus de place possible à la coopération, à la discussion, au conscensus et à la progression incrémentale vers les objectifs du collectif.

## Article 3 — Valeurs

Les critères sur lesquels développer la coopérative sont :
- la pérennité de l'activité au-delà des compromis
- l'indépendance
- la reproductibilité des productions scientifiques
- la solidarité, en particuliers avec les minorités invisibilisées

## Article 4 — Vie de la gouvernance

Chacun·e des membres est consulté pour chacune des décisions à prendre. La délégation de vote pour des groupes de consultations a vocation à fluidifier les consultations et peut être modifiée à tout moment.

Les Assemblées Générales sont une consultation de grande ampleur.

## Capital social

## 

## Dissolution

