# Quels sont les avantages apportés par le statut d'entreprise CAE

Une Coopérative d'Activité et d'Emploi permet à des individus d'avoir une activité indépendante tout en faisant partie d'une entreprise mutualisant une partie des risques. Le régime de sécurité sociale de rattachement est celui de l'activité majoritaire (régime général ou Mutuelle sociale agricole).

# Ne s'agit-il pas de salariat déguisé ?

Les travailleu·se·r·s de la recherche ont vocation à exercer dans différentes fermes sans attache spécifique et à temps partiel, ce qui écarte le salariat déguisé.

Les travailleur·se·s agricoles exercent des activités de recherche à temps partiel et sans vocation à l'exclusivité de cette activité.

# La CAE verse-t-elle des salaires ?

La CAE n'a pas vocation à verser de salaire. En revanche, la CAE peut faire appel à ses membres pour la réalisation de prestations, et par conséquent, donner lieu à un paiement de la CAE vers des membres. La CAE a vocation à coordonner de cette manière une partie de l'activité des membres.

Par ailleurs, les membres sont autonomes économiquement et peuvent répondre à des commandes de la part d'autres organismes, privés ou publics. Dans ce cas, les contrats sont liés directement entre ces derniers et l'individu et donnent lieu à un versement d'argent.

# Tout paiement fait par un client donne-t-il lieu à un versement sous forme de salaire

Non. Tout paiement fait par un client donne lieu à une entrée d'argent sur le compte individuel. À partir du montant présent sur ce compte individuel, un salaire est fixé et versé à l'individu. Le montant sur le compte peut augmenter et le salaire rester stable. D'autres cas sont à prévoir et réglé avec bon sens.


# Quelles sont les sources de financement pour les salaires ?

Chaque membre de la CAE est autonome sur l'obtention de financements pour sa propre activité. La CAE en son nom a également vocation a obtenir des financements, qui donnent lieu à des commandes auprès de ses membres et au règlement de factures.

Les sources de financement peuvent être diverses : commandes privées, financements de recherche publique, déduction du Crédit Impôt Recherche, dons de particuliers, financement participatif, etc


# Qu'en est-il de la participation au capital de la coopérative

Au plus tard après 3 ans en tant qu'entrepreneur·se-salarié, l'entrée en tant que membre associé votant donne lieu à l'achat d'une part sociale ou plus.

# Comment sont prises les décisions

Un·e associé = une voix.

Le mode de gouvernance est un choix pris en commun et influencé par les choix des premier·e·s membres. Une piste est la prise de décision liquide (cf démocratie par proxy).
