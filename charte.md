# Charte

Chaque membre de la coopérative s'engage à respecter et à mettre en pratique les règles suivantes :


1. l'activité au sein de la coopérative est orientée vers le bien-être de ses membres et la solidarité entre ses membres.
2. tout contenu produit au sein de la coopérative et auquel s'applique le droit d'auteur sera libéré sous licence libre au moment de la diffusion publique.
3. les différentes formes d'activité économique cohabitent au sein des activités des membres et les activités à visée économique ont pour but de pérenniser l'activité des membres. L'activité scientifique a pour but de faire progresser les connaissances et techniques scientifiques
4. chaque membre est indépendant·e en termes de gouvernance (en l'absence de contrats collectifs) et chacun·e est invité·e à contribuer à des projets collectifs, pour lesquels, l'indépendance est celle du groupe et chacun·e est solidaire du groupe pour la réalisation des objectifs communs.
