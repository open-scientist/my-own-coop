# Liste de profils invités à se lancer dans la concrétisation de cette exploration

- [profil](profil/loin-de-la-recherche.md) individu travaillant loin de la recherche et souhaitant ouvrir progressivement une activité de recherche
- [profil](profil/postdoc-en-transition.md) post-doc en transition
- [profil](profil/accompagnement-de-projets.md) accompagnat·rice·eur de projets, développeurs de coopérations et donc de coopératives

Chacun de ces profils est une invitation à se lancer !
