# Point de départ

De nombreuses Coopératives d'Activité et d'Emploi existent et il est possible pour un individu souhaitant exercer une activité de recherche scientifique de se lancer en indépendant au sein de l'une d'entre elles. Les limitations viendront de l'assurance professionnelle, qui doit couvrir les activités de recherche. Certaines activités (recherche en informatique, biostatistiques, etc) seront couvertes par les assurances, tandis que les activités expérimentales sur du vivant poseront problème.

Les accompagnant·e·s de projets de différents horizons peuvent trouver leur compte dans l'accompagnement de projets ayant une vocation scientifique.

## Concrètement

L'accompagnement de projets scientifiques peut être fait par une personne dédiant son activité à cette tâche comme elle peut être réalisée par les membres de la coopérative en tant qu'activité mutualisée.

## Contribution

Une personne dédiant une grande part de son activité à l'accompagnement des projets de recherche scientifique des membres simplifiera la réalisation de certaines tâches tout en assurant leur exécution efficace.


