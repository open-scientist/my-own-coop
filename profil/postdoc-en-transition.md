# Profil : post-doc en transition

## Point de départ

À la suite de la thèse, les jeunes chercheu·se·r·s sont généralement embauché·e·s pour des contrats à durée déterminée dits contrats post-doctoraux. Au cours de cette période en CDD, ces personnes sont invité·e·s à passer les concours d'entrée dans la fonction publique, afin d'obtenir un poste permanent en tant que chercheu·r·se ou maitr·e·sse de conférence.

La loi dite "loi Sauvadet" limite à 4 années au cours des 6 dernières années la durée de travail en tant qu'employé·e contractuel·le de la fonction publique au sein d'un même organisme. Par ailleurs, le nombre de postes de fonctionnaires de la recherche scientifique est très inférieur au nombre de post-docs. Par conséquent, de très nombreuses personnes verront leur carrière être interrompue alors que ces personnes sont compétentes pour les travaux qu'elles réalisent.


## Concrètement

De nombreu·se·x post-docs ont de très hautes qualifications et des compétences pour travailler dans la recherche scientifique ainsi que dans des domaines économiques connexes à la recherche.

Une enquête est prévue pour évaluer la prévalence de ce profil et l'adéquation de la proposition de travail en indépendant·e au sein d'une coopérative pour la pérennisation de l'activité de ces personnes.

## Contribution

Ces personnes peuvent poursuivre leur activité en indépendant·e au sein de la coopérative et travailler en tant que prestataires pour le laboratoire d'origine, afin d'exercer leurs compétences et assurer leur pérennité économique (le temps de travail est limité à 75% de leur temps de travail total). Leur activité peut se diversifier par le développement auprès de nouveaux clients tout en ayant une partie des revenus déjà établis.


