# Profil: travaillant loin de la recherche et souhaitant faire de la recherche

## Points de départ

La recherche scientifique est une activité pour qui le souhaite.

Assembler au sein d'une coopérative des travailleurs indépendants ayant une activité viable permet à la coopérative d'avoir une stabilité

et cette stabilité peut être mise à profit pour dégager du temps et de l'énergie à dédier à des activités de recherche.

Un critère qui s'impose est celui de la qualité. L'invitation est faite de construire une activité de recherche sur une activité technique stable. Pour des personnes reprenant pied dans la recherche, une phase de production en-dessous du seuil de qualité est attendue et doit être mise à profit pour se former, afin de passer à une étape de réalisation d'expériences et analyses reproductibles et statistiquement significatives.


## Concrètement ?

Enquête en cours pour évaluer la force du signal sur ce type de profil : à quel point est-il courant ?

## Contribution

Toute personne en cours d'activité au sein d'une entreprise souhaitant se lancer en indépendant au sein d'une coopérative d'indépendants pour ouvrir petit à petit une activité de recherche (passage de mono à poly activité) est invitée à venir rejoindre la coopérative.
