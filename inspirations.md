# Sources d'inspirations

Par leurs propos, leurs idées, leur engagement, leur disponibilité pour l'échange et la réflexion commune.

[Manufacture coopérative](http://manufacture.coop/)

[HackYourPhD](hackyourphd.org)

[Coopaname](coopaname.coop)

[La Paillasse](lapaillasse.org)

[Pointcarré](pointcarre.coop)

[Coopetic](coopetic-recherche.com)

[OpenLaw](openlaw.fr) Discussion ([1])(https://twitter.com/jibe_jeybee/status/961047873629704198)
